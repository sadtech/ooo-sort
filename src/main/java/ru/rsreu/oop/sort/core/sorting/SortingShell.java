package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Lazy
public class SortingShell extends AlgorithmSort<Double> {

    public SortingShell() {
        super.setTypeSort(TypeSort.SORTING_SHELL);
    }

    @Override
    public List<Double> sort(List<Double> numbers) {
        double temp;
        for (int step = numbers.size() / 2; step > 0; step /= 2)
            for (int i = step; i < numbers.size(); i++) {
                temp = numbers.get(i);
                int j;
                for (j = i; j >= step; j -= step) {
                    if (temp < numbers.get(j - step))
                        numbers.set(j, numbers.get(j - step));
                    else
                        break;
                }
                numbers.set(j, temp);
            }
        return numbers;
    }
}
