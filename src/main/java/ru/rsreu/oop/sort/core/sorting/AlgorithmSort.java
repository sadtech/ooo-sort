package ru.rsreu.oop.sort.core.sorting;

public abstract class AlgorithmSort<N extends Number> implements Sorting<N> {

    private TypeSort typeSort;

    public TypeSort getTypeSort() {
        return typeSort;
    }

    public void setTypeSort(TypeSort typeSort) {
        this.typeSort = typeSort;
    }
}
