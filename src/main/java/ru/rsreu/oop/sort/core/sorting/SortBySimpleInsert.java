package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Lazy
public class SortBySimpleInsert extends AlgorithmSort<Double> {

    public SortBySimpleInsert() {
        super.setTypeSort(TypeSort.SORT_BY_SIMPLE_INSERT);
    }

    @Override
    public List<Double> sort(List<Double> numbers) {
        double temp;
        int j;
        for (int i = 0; i < numbers.size() - 1; i++) {
            if (numbers.get(i) > numbers.get(i + 1)) {
                temp = numbers.get(i + 1);
                numbers.set(i + 1, numbers.get(i));
                j = i;
                while (j > 0 && temp < numbers.get(j - 1)) {
                    numbers.set(j, numbers.get(j - 1));
                    j--;
                }
                numbers.set(j, temp);
            }
        }
        return numbers;
    }
}
