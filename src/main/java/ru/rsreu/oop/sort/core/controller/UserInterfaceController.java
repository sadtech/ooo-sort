package ru.rsreu.oop.sort.core.controller;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rsreu.oop.sort.core.data.FileOpenService;
import ru.rsreu.oop.sort.core.data.FileSaveService;
import ru.rsreu.oop.sort.core.exception.NotFound;
import ru.rsreu.oop.sort.core.sorting.AlgorithmSort;
import ru.rsreu.oop.sort.core.sorting.TypeSort;
import ru.rsreu.oop.sort.core.utils.MessageAlert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.concurrent.TimeUnit.NANOSECONDS;


public class UserInterfaceController {
    private List<Double> numbers;
    private List<Double> result;
    private int selectedMenuItem = 0;
    private String[] lines = {"Сортировка простыми вставками", "Сортировка простым выбором", "Сортировка простым обменом",
            "Сортировка методом Шелла", "Метод квадратичной выборки", "Алгоритм \"быстрой сортировки\"",
            "Сортировка методом слияния"};
    @FXML
    Label resultTime;
    @FXML
    ListView initialListView;
    @FXML
    ListView sortingListView;
    @FXML
    ChoiceBox sortSelection;
    @FXML
    Button sorting;
    @FXML
    Menu menu;

    @Autowired
    private FileSaveService fileSaveService;
    @Autowired
    private FileOpenService fileOpenService;
    @Autowired
    private List<AlgorithmSort> sortings;

    @FXML
    private void initialize() {
        sortSelection.setItems(FXCollections.observableList(Arrays.asList(lines)));
        sortSelection.getSelectionModel().select(0);
        setControlStatus(true);
        menu.setOnAction(event -> selectedMenuItem = Arrays.asList(lines).indexOf(((MenuItem) event.getTarget()).getText()));
    }

    @FXML
    void newFile() {
        if (numbers != null)
            numbers.clear();
        initialListView.getItems().clear();
        sortingListView.getItems().clear();
        setControlStatus(true);
    }

    @FXML
    void openFile() {
        fileOpenService.openFile();
        numbers = fileOpenService.getNumbers();
        initialListView.setItems(FXCollections.observableArrayList(numbers));
        if (numbers.size() > 0) {
            setControlStatus(false);
        }
    }

    @FXML
    void saveFile() {
        if (sortingListView.getItems().size() > 0) {
            if (MessageAlert.showMessage(Alert.AlertType.CONFIRMATION, "Сохранить изменения?").getResult() == ButtonType.YES) {
                fileSaveService.setNumbers(sortingListView.getItems());
                fileSaveService.saveFile();
            }
        } else {
            MessageAlert.showMessage(Alert.AlertType.ERROR, "Данные для сохранения отсутствуют!");
        }
    }

    @FXML
    void close() {
        if (sortingListView.getItems().size() > 0) {
            if (MessageAlert.showMessage(Alert.AlertType.CONFIRMATION, "Сохранить изменения?").getResult() == ButtonType.YES) {
                fileSaveService.setNumbers(sortingListView.getItems());
                fileSaveService.saveFile();
            }
        }
        System.exit(0);
    }

    @FXML
    private void runSorting() {
        if (!sortSelection.isDisable()) {
            sortSelection.getSelectionModel().select(selectedMenuItem);
            sorting();
        }
    }

    @FXML
    private void sorting() {
        result = new ArrayList<>(numbers);
        long start = System.nanoTime();
        selectSorting();
        showTime(start);
    }

    private TypeSort typeSort(Integer number) {
        switch (number) {
            case 0:
                return TypeSort.SORT_BY_SIMPLE_INSERT;
            case 1:
                return TypeSort.SIMPLE_SORTING;
            case 2:
                return TypeSort.SORT_DIRECT_EXCHANGE;
            case 3:
                return TypeSort.SORTING_SHELL;
            case 4:
                return TypeSort.METHOD_QUADRATIC_SAMPLING;
            case 5:
                return TypeSort.ALGORITHM_QUICK_SORT;
            case 6:
                return TypeSort.MERGE_SORT_INNER;
            default:
                MessageAlert.showMessage(Alert.AlertType.ERROR, "Неизвестный код!");
                throw new NotFound();
        }
    }

    private void selectSorting() {
        TypeSort typeSort = typeSort(sortSelection.getSelectionModel().getSelectedIndex());
        AlgorithmSort algorithmSort = sortings.parallelStream()
                .filter(sorting1 -> typeSort.equals(sorting1.getTypeSort()))
                .findFirst().orElseThrow(NotFound::new);
        List sort;
        switch (typeSort) {
            case ALGORITHM_QUICK_SORT:
                sort = algorithmSort.sort(result, 0, result.size() - 1);
                break;
            case MERGE_SORT_INNER:
                sort = algorithmSort.sort(result, createArrayList(result.size()), 0, result.size());
                break;
            default:
                sort = algorithmSort.sort(result);
                break;
        }
        sortingListView.setItems(FXCollections.observableArrayList(sort));
    }

    private void showTime(long start) {
        long finish = System.nanoTime();
        resultTime.setVisible(true);
        resultTime.setText(String.valueOf("Время выполнения: ") +
                NANOSECONDS.toMicros(finish - start) +
                "мкс");
    }

    private List<Double> createArrayList(int size) {
        ArrayList<Double> arrayList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            arrayList.add(0.0);
        }
        return arrayList;
    }

    private void setControlStatus(boolean status) {
        sortSelection.setDisable(status);
        sorting.setDisable(status);
        menu.setDisable(status);
    }
}
