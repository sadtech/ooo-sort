package ru.rsreu.oop.sort.core.data;

import javafx.collections.ObservableList;

public interface SaveService {
    void saveFile();

    void setNumbers(ObservableList<Double> numbers);
}
