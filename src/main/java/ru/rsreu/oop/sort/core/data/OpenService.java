package ru.rsreu.oop.sort.core.data;

import java.util.List;

public interface OpenService {
    void openFile();
    List<Double> getNumbers();
}
