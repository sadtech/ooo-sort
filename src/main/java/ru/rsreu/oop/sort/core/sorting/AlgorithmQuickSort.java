package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Lazy
public class AlgorithmQuickSort extends AlgorithmSort<Double> {

    public AlgorithmQuickSort() {
        super.setTypeSort(TypeSort.ALGORITHM_QUICK_SORT);
    }

    @Override
    public List<Double> sort(List<Double> list, int low, int high) {
        int i = low;
        int j = high;
        double k = list.get(low);
        while (low < high) {
            while ((list.get(high) >= k) && (low < high)) high--;
            if (low != high) {
                list.set(low, list.get(high));
                low++;
            }
            while ((list.get(low) <= k) && (low < high)) low++;
            if (low != high) {
                list.set(high, list.get(low));
                high--;
            }
        }
        list.set(low, k);
        int t = low;
        low = i;
        high = j;
        if (low < t)
            sort(list, low, t - 1);
        if (high > t)
            sort(list, t + 1, high);
        return list;
    }
}
