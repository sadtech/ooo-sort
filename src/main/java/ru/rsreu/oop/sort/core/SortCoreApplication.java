package ru.rsreu.oop.sort.core;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.rsreu.oop.sort.core.config.ConfigurationControllers;

@SpringBootApplication
public class SortCoreApplication extends AbstractJavaFxApplicationSupport {

    @Value("${ui.title:Сортировка}")//
    private String windowTitle;

    @Autowired
    private ConfigurationControllers.View view;

    @Override
    public void start(Stage stage) {
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(view.getParent()));
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.show();
    }

    public static void main(String[] args) {
        launchApp(SortCoreApplication.class, args);
    }

}
