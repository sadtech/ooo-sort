package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Lazy
public class SimpleSorting extends AlgorithmSort<Double> {

    public SimpleSorting() {
        super.setTypeSort(TypeSort.SIMPLE_SORTING);
    }

    @Override
    public List<Double> sort(List<Double> numbers) {
        for (int i = 0; i < numbers.size() - 1; i++) {
            int index = i;
            for (int j = i + 1; j < numbers.size(); j++) {
                if (numbers.get(j) < numbers.get(index))
                    index = j;
            }
            double temp = numbers.get(i);
            numbers.set(i, numbers.get(index));
            numbers.set(index, temp);
        }
        return numbers;
    }
}
