package ru.rsreu.oop.sort.core.config;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rsreu.oop.sort.core.controller.UserInterfaceController;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class ConfigurationControllers {

    private static final String URL_UI = "fxml/main.fxml";

    @Bean(name = "mainView")
    public View getMainView() throws IOException {
        return loadView();
    }

    @Bean
    public UserInterfaceController getMainController() throws IOException {
        return (UserInterfaceController) getMainView().getController();
    }

    private View loadView() throws IOException {
        try (InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream(URL_UI)) {
            FXMLLoader loader = new FXMLLoader();
            loader.load(fxmlStream);
            return new View(loader.getRoot(), loader.getController());
        }
    }

    public class View {
        private Parent parent;
        private Object controller;

        public View(Parent parent, Object controller) {
            this.parent = parent;
            this.controller = controller;
        }

        public Parent getParent() {
            return parent;
        }

        public void setParent(Parent parent) {
            this.parent = parent;
        }

        public Object getController() {
            return controller;
        }

        public void setController(Object controller) {
            this.controller = controller;
        }
    }

}