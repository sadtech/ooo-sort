package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Lazy
public class MethodQuadraticSampling extends AlgorithmSort<Double> {

    private static double MAX_SAFE_VALUE = Math.pow(2, sun.misc.DoubleConsts.SIGNIFICAND_WIDTH) - 1;

    private Integer[] nums; //Коллекция "номерков"
    private Double[] values; //Колекция выбранных переменных

    public MethodQuadraticSampling() {
        super.setTypeSort(TypeSort.METHOD_QUADRATIC_SAMPLING);
    }

    @Override
    public List<Double> sort(List<Double> numbers) {
        int numGroup = (int) Math.sqrt(numbers.size()), numElem = numbers.size() / numGroup;
        nums = new Integer[numElem];
        values = new Double[numElem];
        List<Double> copy = numbers;
        List<Double> res = new ArrayList<>();
        int why; // индекс переменной, которую запишем в массив
        for (int i = 0; i < numGroup; i++)
            findMinElemToSquart(numElem, copy, i);
        for (int i = 0; i < numbers.size(); i++) {
            why = findIndexOfMinValue(numGroup);
            res.add(i, values[why]);
            copy.set(why * numElem + nums[why], 10000.0);
            values[why] = 10000.0;
            findMinElemToSquart(numElem, copy, why);
        }
        return res;
    }

    private void findMinElemToSquart(int numElem, List<Double> copy, int e) {
        double min = MAX_SAFE_VALUE;
        for (int i = 0; i < numElem; i++) {
            if ((min > copy.get(e * numElem + i)) && (copy.get(e * numElem + i) != 10000.0)) {
                min = copy.get(e * numElem + i);
                nums[e] = i;
            }
            values[e] = copy.get(e * numElem + nums[e]);
        }
    }

    private int findIndexOfMinValue(int size) {
        double min = MAX_SAFE_VALUE;
        int temp = 0;
        for (int i = 0; i < size; i++) {
            if (min > values[i] && (values[i] != 10000.0)) {
                min = values[i];
                temp = i;
            }
        }
        return temp;
    }
}
