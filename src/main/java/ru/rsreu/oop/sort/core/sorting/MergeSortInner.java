package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Lazy
public class MergeSortInner extends AlgorithmSort<Double> {

    public MergeSortInner() {
        super.setTypeSort(TypeSort.MERGE_SORT_INNER);
    }

    @Override
    public List<Double> sort(List<Double> buffer1, List<Double> buffer2, int startIndex, int endIndex) {
        if (startIndex >= endIndex - 1) {
            return buffer1;
        }
        // уже отсортирован.
        int middle = startIndex + (endIndex - startIndex) / 2;
        List<Double> sorted1 = sort(buffer1, buffer2, startIndex, middle);
        List<Double> sorted2 = sort(buffer1, buffer2, middle, endIndex);

        // Слияние
        int index1 = startIndex;
        int index2 = middle;
        int destIndex = startIndex;
        List<Double> result = sorted1 == buffer1 ? buffer2 : buffer1;
        while (index1 < middle && index2 < endIndex) {
            result.set(destIndex++, sorted1.get(index1) < sorted2.get(index2)
                    ? sorted1.get(index1++) : sorted2.get(index2++));
        }
        while (index1 < middle) {
            result.set(destIndex++, sorted1.get(index1++));
        }
        while (index2 < endIndex) {
            result.set(destIndex++, sorted2.get(index2++));
        }
        return result;
    }
}
