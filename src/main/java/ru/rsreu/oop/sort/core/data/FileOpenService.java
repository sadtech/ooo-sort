package ru.rsreu.oop.sort.core.data;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import org.springframework.stereotype.Service;
import ru.rsreu.oop.sort.core.utils.MessageAlert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Service
public class FileOpenService implements OpenService {

    private List<Double> numbers = new ArrayList<>();

    @Override
    public void openFile() {
        String folder;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите текстовый файл");
        File directory = fileChooser.showOpenDialog(null);
        if (directory != null) {
            folder = directory.getAbsolutePath();
            readNumber(folder);
        }
    }

    private void readNumber(final String folder) {
        try {
            File file = new File(folder);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                if (scanner.hasNextDouble()) {
                    numbers.add(scanner.nextDouble());
                } else {
                    scanner.next();
                }
            }

            if (!numbers.isEmpty())
                MessageAlert.showMessage(Alert.AlertType.INFORMATION, "Числа успешно считаны.");
            else {
                MessageAlert.showMessage(Alert.AlertType.ERROR, "Числа не найдены в файле!");
            }
        } catch (FileNotFoundException e) {
            MessageAlert.showMessage(Alert.AlertType.ERROR, "Файл не найден!");
        }
    }

    public List<Double> getNumbers() {
        return numbers;
    }

}
