package ru.rsreu.oop.sort.core.sorting;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Lazy
public class SortDirectExchange extends AlgorithmSort<Double> {

    public SortDirectExchange() {
        super.setTypeSort(TypeSort.SORT_DIRECT_EXCHANGE);
    }

    @Override
    public List<Double> sort(List<Double> numbers) {
        for (int i = 0; i < numbers.size() - 1; i++) {
            for (int j = (numbers.size() - 1); j > i; j--) {
                if (numbers.get(j - 1) > numbers.get(j)) {
                    double temp = numbers.get(j - 1);
                    numbers.set(j - 1, numbers.get(j));
                    numbers.set(j, temp);
                }
            }
        }
        return numbers;
    }
}
