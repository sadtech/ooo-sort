package ru.rsreu.oop.sort.core.sorting;

import java.util.List;

public interface Sorting<N extends Number> {

    default List<N> sort(List<N> numbers) {
        throw new IllegalStateException();
    }

    default List<N> sort(List<N> list, int low, int high) {
        throw new IllegalStateException();
    }

    default List<N> sort(List<N> buffer1, List<Double> buffer2, int startIndex, int endIndex) {
        throw new IllegalStateException();
    }
}
