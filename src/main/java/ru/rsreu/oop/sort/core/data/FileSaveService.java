package ru.rsreu.oop.sort.core.data;

import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.stage.DirectoryChooser;
import org.springframework.stereotype.Service;
import ru.rsreu.oop.sort.core.utils.MessageAlert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileSaveService implements SaveService {

    private List<Double> numbers;

    public void saveFile() {
        String folder = "";
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Выберите директорию");
        File dir = directoryChooser.showDialog(null);
        if (dir != null) {
            folder = dir.getAbsolutePath();
        }
        writeDouble(folder, "sorting_nums.txt");
    }

    public void writeDouble(String directory, String name){
        try (FileWriter fos = new FileWriter(directory+"\\"+name)) {
            fos.write(convertNumbersToLine());
            MessageAlert.showMessage(Alert.AlertType.INFORMATION, "Запись в файл выполнена");
        } catch (FileNotFoundException e) {
            MessageAlert.showMessage(Alert.AlertType.ERROR, "Cann't open file!");
        } catch(IOException e){
            MessageAlert.showMessage(Alert.AlertType.ERROR,"Ошибка ввода/вывода");
        }
    }

    public void setNumbers(ObservableList<Double> numbers) {
        this.numbers = new ArrayList<>(numbers);
    }


    public String convertNumbersToLine(){
        StringBuilder line = new StringBuilder();
        for (double number : numbers){
            line.append(new StringBuilder(Double.toString(number)).append(" "));
        }
        return line.toString();
    }
}
